<%-- 
    Document   : home2
    Created on : Nov 19, 2020, 2:39:54 PM
    Author     : NhatHuu
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
        <jsp:include page="../admin/include/css.jsp" />
        <link rel="stylesheet" href="<c:url value="/resources-management/style.css"/>">
    </head>
    <body>
        <jsp:include page="include/menu_left.jsp" />
        <div class="all-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="logo-pro">
                            <a href="index.html"><img class="main-logo" src="<c:url value="/resources-management/img/logo/logo.png"/>" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-advance-area">
                <jsp:include page="include/header_top.jsp" />
                <div class="breadcome-area">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="breadcome-list">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <div class="breadcomb-wp">
                                                <div class="breadcomb-icon">
                                                    <i class="icon nalika-home"></i>
                                                </div>
                                                <div class="breadcomb-ctn">
                                                    <h2>Dashboard One</h2>
                                                    <p>Welcome to Huu <span class="bread-ntd">Admin Template</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-admin container-fluid">
                <div class="row admin text-center">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <!--//show total product in shop--> 
                                <div class="admin-content analysis-progrebar-ctn res-mg-t-15">
                                    <h4 class="text-left text-uppercase"><b>Products</b></h4>
                                    <div class="row vertical-center-box vertical-center-box-tablet">
                                        <div class="col-xs-3 mar-bot-15 text-left">

                                        </div>
                                        <div class="col-xs-9 cus-gh-hd-pro">
                                            <h2 class="text-right no-margin">10,023</h2>
                                        </div>
                                    </div>
                                    <!--                                    <div class="progress progress-mini">
                                                                            <div style="width: 83%;" class="progress-bar bg-green"></div>
                                                                        </div>-->
                                </div>
                            </div>
                            <!--total user-->
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-bottom:1px;">
                                <div class="admin-content analysis-progrebar-ctn res-mg-t-30">
                                    <h4 class="text-left text-uppercase"><b>All Pending Orders</b></h4>
                                    <div class="row vertical-center-box vertical-center-box-tablet">
                                        <div class="text-left col-xs-3 mar-bot-15">
                                        </div>
                                        <div class="col-xs-9 cus-gh-hd-pro">
                                            <h2 class="text-right no-margin">50</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <div class="admin-content analysis-progrebar-ctn res-mg-t-30">
                                    <h4 class="text-left text-uppercase"><b>Users</b></h4>
                                    <div class="row vertical-center-box vertical-center-box-tablet">
                                        <div class="text-left col-xs-3 mar-bot-15">
                                            <!--<label class="label bg-blue">50% <i class="fa fa-level-up" aria-hidden="true"></i></label>-->
                                        </div>
                                        <div class="col-xs-9 cus-gh-hd-pro">
                                            <h2 class="text-right no-margin">123</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <div class="admin-content analysis-progrebar-ctn res-mg-t-30">
                                    <h4 class="text-left text-uppercase"><b>Total Revenue </b></h4>
                                    <div class="row vertical-center-box vertical-center-box-tablet">
                                        <div class="text-left col-xs-3 mar-bot-15">
                                            <!--<label class="label bg-purple">80% <i class="fa fa-level-up" aria-hidden="true"></i></label>-->
                                        </div>
                                        <div class="col-xs-9 cus-gh-hd-pro">
                                            <h2 class="text-right no-margin">$100,000</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <%--<jsp:include page="include/foodter.jsp" />--%>
        </div>

        <jsp:include page="include/js_management.jsp" />
    </body>
</html>
