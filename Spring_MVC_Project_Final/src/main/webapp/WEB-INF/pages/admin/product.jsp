<%-- 
    Document   : product
    Created on : Nov 19, 2020, 9:12:18 PM
    Author     : NhatHuu
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
        <jsp:include page="include/css.jsp" />
        <link rel="stylesheet" href="<c:url value="/resources-management/style.css"/>">
    </head>
    <body>
        <jsp:include page="include/menu_left.jsp" />
        <div class="all-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="logo-pro">
                            <a href="index.html"><img class="main-logo" src="<c:url value="/resources-management/img/logo/logo.png"/>" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-advance-area">
                <jsp:include page="include/header_top.jsp" />
                <div class="breadcome-area">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="breadcome-list">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <div class="breadcomb-wp">
                                                <div class="breadcomb-icon">
                                                    <i class="icon nalika-home"></i>
                                                </div>
                                                <div class="breadcomb-ctn">
                                                    <h2>Product</h2>
                                                    <p>Welcome to Huu <span class="bread-ntd">Admin Template</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-product-tab-area mg-b-30">
                <!-- Single pro tab review Start-->
                <div class="single-pro-review-area">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="review-tab-pro-inner">
                                    <ul id="myTab3" class="tab-review-design">
                                        <li class="active"><a href="#description"><i class="icon nalika-edit" aria-hidden="true"></i> Product </a></li>
                                        <li><a href="#reviews"><i class="icon nalika-picture" aria-hidden="true"></i> Pictures</a></li>
                                    </ul>
                                    <div id="myTabContent" class="tab-content custom-product-edit">
                                        <div class="product-tab-list tab-pane fade active in" id="description">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="review-content-section">
                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="icon nalika-user" aria-hidden="true"></i></span>
                                                            <input type="text" class="form-control" placeholder="First Name">
                                                        </div>
                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="icon nalika-edit" aria-hidden="true"></i></span>
                                                            <input type="text" class="form-control" placeholder="Product Title">
                                                        </div>
                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="fa fa-usd" aria-hidden="true"></i></span>
                                                            <input type="text" class="form-control" placeholder="Regular Price">
                                                        </div>
                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="icon nalika-new-file" aria-hidden="true"></i></span>
                                                            <input type="text" class="form-control" placeholder="Tax">
                                                        </div>
                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="icon nalika-favorites" aria-hidden="true"></i></span>
                                                            <input type="text" class="form-control" placeholder="Quantity">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="review-content-section">
                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="icon nalika-user" aria-hidden="true"></i></span>
                                                            <input type="text" class="form-control" placeholder="Last Name">
                                                        </div>
                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="icon nalika-favorites-button" aria-hidden="true"></i></span>
                                                            <input type="text" class="form-control" placeholder="Product Description">
                                                        </div>
                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="fa fa-usd" aria-hidden="true"></i></span>
                                                            <input type="text" class="form-control" placeholder="Sale Price">
                                                        </div>
                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="icon nalika-like" aria-hidden="true"></i></span>
                                                            <input type="text" class="form-control" placeholder="Category">
                                                        </div>
                                                        <select name="select" class="form-control pro-edt-select form-control-primary">
                                                            <option value="opt1">Select One Value Only</option>
                                                            <option value="opt2">2</option>
                                                            <option value="opt3">3</option>
                                                            <option value="opt4">4</option>
                                                            <option value="opt5">5</option>
                                                            <option value="opt6">6</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="text-center custom-pro-edt-ds">
                                                        <button type="button" class="btn btn-ctl-bt waves-effect waves-light m-r-10">Save
                                                        </button>
                                                        <button type="button" class="btn btn-ctl-bt waves-effect waves-light">Discard
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-tab-list tab-pane fade" id="reviews">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="review-content-section">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="pro-edt-img">
                                                                    <img src="img/new-product/5-small.jpg" alt="" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="product-edt-pix-wrap">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon">TT</span>
                                                                                <input type="text" class="form-control" placeholder="Label Name">
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-lg-6">
                                                                                    <div class="form-radio">
                                                                                        <form>
                                                                                            <div class="radio radiofill">
                                                                                                <label>
                                                                                                    <input type="radio" name="radio"><i class="helper"></i>Largest Image
                                                                                                </label>
                                                                                            </div>
                                                                                            <div class="radio radiofill">
                                                                                                <label>
                                                                                                    <input type="radio" name="radio"><i class="helper"></i>Medium Image
                                                                                                </label>
                                                                                            </div>
                                                                                            <div class="radio radiofill">
                                                                                                <label>
                                                                                                    <input type="radio" name="radio"><i class="helper"></i>Small Image
                                                                                                </label>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-6">
                                                                                    <div class="product-edt-remove">
                                                                                        <button type="button" class="btn btn-ctl-bt waves-effect waves-light">Remove
                                                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="pro-edt-img">
                                                                    <img src="img/new-product/6-small.jpg" alt="" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="product-edt-pix-wrap">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon">TT</span>
                                                                                <input type="text" class="form-control" placeholder="Label Name">
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-lg-6">
                                                                                    <div class="form-radio">
                                                                                        <form>
                                                                                            <div class="radio radiofill">
                                                                                                <label>
                                                                                                    <input type="radio" name="radio"><i class="helper"></i>Largest Image
                                                                                                </label>
                                                                                            </div>
                                                                                            <div class="radio radiofill">
                                                                                                <label>
                                                                                                    <input type="radio" name="radio"><i class="helper"></i>Medium Image
                                                                                                </label>
                                                                                            </div>
                                                                                            <div class="radio radiofill">
                                                                                                <label>
                                                                                                    <input type="radio" name="radio"><i class="helper"></i>Small Image
                                                                                                </label>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-6">
                                                                                    <div class="product-edt-remove">
                                                                                        <button type="button" class="btn btn-ctl-bt waves-effect waves-light">Remove
                                                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="pro-edt-img mg-b-0">
                                                                    <img src="img/new-product/7-small.jpg" alt="" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="product-edt-pix-wrap">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon">TT</span>
                                                                                <input type="text" class="form-control" placeholder="Label Name">
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-lg-6">
                                                                                    <div class="form-radio">
                                                                                        <form>
                                                                                            <div class="radio radiofill">
                                                                                                <label>
                                                                                                    <input type="radio" name="radio"><i class="helper"></i>Largest Image
                                                                                                </label>
                                                                                            </div>
                                                                                            <div class="radio radiofill">
                                                                                                <label>
                                                                                                    <input type="radio" name="radio"><i class="helper"></i>Medium Image
                                                                                                </label>
                                                                                            </div>
                                                                                            <div class="radio radiofill">
                                                                                                <label>
                                                                                                    <input type="radio" name="radio"><i class="helper"></i>Small Image
                                                                                                </label>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-6">
                                                                                    <div class="product-edt-remove">
                                                                                        <button type="button" class="btn btn-ctl-bt waves-effect waves-light">Remove
                                                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <jsp:include page="include/foodter.jsp" />
        </div>

        <jsp:include page="include/js_management.jsp" />
    </body>
</html>
