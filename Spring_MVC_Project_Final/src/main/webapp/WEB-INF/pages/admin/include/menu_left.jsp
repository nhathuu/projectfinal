<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="left-sidebar-pro">
    <nav id="sidebar" class="">
        <div class="sidebar-header">
            <a href="index.html"><img class="main-logo" src="<c:url value="/resources-management/img/logo/logo.png"/>" alt="" /></a>
            <strong><img src="<c:url value="/resources-management/img/logo/logosn.png"/>" alt="" /></strong>
        </div>
        <div class="nalika-profile">
            <div class="profile-dtl">
                <a href="#"><img src="<c:url value="/resources-management/img/notification/4.jpg"/>" alt="" /></a>
                <h2>Nhat <span class="min-dtn">Huu</span></h2>
            </div>
            <div class="profile-social-dtl">
                <ul class="dtl-social">
                    <li><a href="#"><i class="icon nalika-facebook"></i></a></li>
                    <li><a href="#"><i class="icon nalika-twitter"></i></a></li>
                    <li><a href="#"><i class="icon nalika-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="left-custom-menu-adp-wrap comment-scrollbar">
            <nav class="sidebar-nav left-sidebar-menu-pro">
                <ul class="metismenu" id="menu1">
                    <li class="active">
                        <a class="has-arrow" href="index.html">
                            <i class="icon nalika-home icon-wrap"></i>
                            <span class="mini-click-non">Ecommerce</span>
                        </a>
                        <ul class="submenu-angle" aria-expanded="true">
                            <li><a title="Dashboard v.1" href="${pageContext.request.contextPath}/admin/home"><span class="mini-sub-pro">Dashboard</span></a></li>
                            <li><a title="Product List" href="${pageContext.request.contextPath}/admin/product-list"><span class="mini-sub-pro">Manage Products</span></a></li>
                            <li><a title="Product List" href="${pageContext.request.contextPath}/admin/product"><span class="mini-sub-pro">Add/Edit Product</span></a></li>
                            <li><a title="Product List" href=""><span class="mini-sub-pro">Manage Categories</span></a></li>
                            <li><a title="Product Cart" href="#"><span class="mini-sub-pro">Manage Orders</span></a></li>
                            <li><a title="Product List" href="#"><span class="mini-sub-pro">Manage Promotion</span></a></li>
                            <li><a title="Product List" href="#"><span class="mini-sub-pro">Manage Users</span></a></li>
                            <li><a title="Product Payment" href="#"><span class="mini-sub-pro">Create Payment</span></a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="has-arrow" href="mailbox.html" aria-expanded="false"><i class="icon nalika-user"></i> <span class="mini-click-non">Account</span></a>
                        <ul class="submenu-angle" aria-expanded="false">
                            <li><a title="Product List" href="#"><span class="mini-sub-pro">Manage Users</span></a></li>
                            <li><a title="Product List" href="#"><span class="mini-sub-pro">Manage Roles</span></a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </nav>
</div>